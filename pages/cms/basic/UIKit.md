## UI Kit
- 詳細文件放置於Zeplin上https://zeplin.io  
- 帳號密碼放置於技術帳號文件內。
___

#### 1.顏色、背景及陰影
![Styleguide](/images/styles/Styleguide.jpg)<br><br>
<br><br>


#### 2.文字(淡色背景)
![Typography Light](/images/styles/TypographyLight.jpg)<br><br>
<br><br>

#### 1.顏色、背景及陰影
![Typography Dark](/images/styles/TypographyDark.jpg)<br><br>
<br><br>

#### 1.元件設計
![Element](/images/styles/Element.jpg)<br><br>
<br><br>