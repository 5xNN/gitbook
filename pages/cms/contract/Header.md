## 網頁Header
![4_0_header](/images/contract/4_0_header.jpg)

**1. 合約審核：**  
- 點擊進入合約審核畫面。<br><br>  

**2. 廠商管理：**  
- 點擊進入廠商管理畫面。<br><br>  

**3. USER：**  
- 顯示文字為使用者名稱。
- 點擊跑出下拉式選單提供登出功能。<br><br><br>